#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Tomasz Pyrek, Marika Wróbel"
__copyright__ = "Tomasz Pyrek, Marika Wróbel"
__license__ = "mit"

import pytest
import os
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from random_forest.RandomForest import RandomForest

def test_abalone():
    if os.path.isfile('test_abalone_result.csv'):
        os.remove('test_abalone_result.csv')
    np.random.seed(0)
    num_lines = sum(1 for line in open('tests/abalone.data'))
    file = open('tests/abalone.data', 'r')
    array = []

    for i in range(0, num_lines):
        line = file.readline().split(',')
        line = [x.strip() for x in line]
        array.append(line)

    abalone_data = {'data': [a[1:] for a in array],
                    'sex': np.array(pd.factorize([a[0] for a in array])[0]),
                    'sex_names': np.array(list(set([a[0] for a in array])), dtype='<U10'),
                    'feature_names': ['Length', 'Diam',	'Height', 'Whole', 'Shucked', 'Viscera', 'Shell', 'Rings']}

    df = pd.DataFrame(abalone_data['data'], columns=abalone_data['feature_names'])
    df['categories'] = abalone_data['sex']
    df['is_train'] = np.random.uniform(0, 1, len(df)) <= 0.75
    train, test = df[df['is_train'] == True], df[df['is_train'] == False]

    features = df.columns[:8]
    clf = RandomForestClassifier(n_jobs=2, random_state=0)
    clf.fit(train[features], train['categories'].values)
    predicted = abalone_data['sex_names'][clf.predict(test[features])]
    predicted = pd.crosstab(abalone_data['sex_names'][test['categories']], predicted, rownames=['Oryginalne'],
                            colnames=['Przewidziane'])

    file = open('test_abalone_result.csv', 'a+')
    file.write('\n\nRESULT - ABALONE\n\n\nSKLEARN\n')
    predicted.to_csv(file, sep='\t', encoding='utf-8')

    print("\n\nRESULT - ABALONE\n\n")
    print("\nSKLEARN\n")
    print(predicted)

    features_and_categories = df.columns[:9]
    random_forest_abalone = RandomForest(train[features_and_categories].values)
    random_forest_abalone.train()
    predict = []

    for t in test[features].values:
        predict.append(abalone_data['sex_names'][random_forest_abalone.predict(t)])

    predict = np.array(predict, dtype='<U10')

    tab_abalone = pd.crosstab(abalone_data['sex_names'][test['categories']], predict, rownames=["Oryginalne"],
                          colnames=["Przewidywane"])

    file.write('\nNASZE\n')
    tab_abalone.to_csv(file, sep='\t', encoding='utf-8')
    file.close()
    print("\n\nNASZE\n")
    print(tab_abalone)
