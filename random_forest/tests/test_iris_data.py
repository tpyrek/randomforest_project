#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Tomasz Pyrek, Marika Wróbel"
__copyright__ = "Tomasz Pyrek, Marika Wróbel"
__license__ = "mit"

import pytest
import os
import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
from random_forest.RandomForest import RandomForest

def test_iris():
    if os.path.isfile('test_iris_result.csv'):
        os.remove('test_iris_result.csv')
    np.random.seed(0)
    iris = load_iris()

    species = pd.Categorical.from_codes(iris.target, iris.target_names)
    df = pd.DataFrame(iris.data, columns=iris.feature_names)

    df['species'] = species
    df['is_train'] = np.random.uniform(0,1,len(df)) <= 0.75
    train, test = df[df['is_train']==True], df[df['is_train']==False]
    features = df.columns[:4]
    y = pd.factorize(train['species'])[0]
    clf = RandomForestClassifier(n_jobs=2, random_state=0)
    clf.fit(train[features], y)
    predicted = iris.target_names[clf.predict(test[features])]
    predicted = pd.crosstab(test['species'], predicted, rownames=['Oryginalne'], colnames=['Przewidziane'])
	
    file = open('test_iris_result.csv', 'a+')
    file.write('\n\nRESULT - IRIS\n\n\nSKLEARN\n')
    predicted.to_csv(file, sep='\t', encoding='utf-8')

    print("\n\nRESULT - IRIS\n\n")
    print("\nSKLEARN\n")
    print(predicted)

    a = df.columns[:5]

    random_forest = RandomForest(train[a].values)
    random_forest.train()
    predict = []

    for t in test[features].values:
        predict.append(random_forest.predict(t))

    predict = np.array(predict, dtype="<U10")
    tab = pd.crosstab(test['species'], predict, rownames=["Oryginalne"], colnames=["Przewidywane"])
    
    file.write('\nNASZE\n')
    tab.to_csv(file, sep='\t', encoding='utf-8')
    file.close()
    print("\nNASZE\n")
    print(tab)
