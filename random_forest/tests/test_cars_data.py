#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Tomasz Pyrek, Marika Wróbel"
__copyright__ = "Tomasz Pyrek, Marika Wróbel"
__license__ = "mit"

import pytest
import os
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from random_forest.RandomForest import RandomForest

def test_cars():
    if os.path.isfile('test_cars_result.csv'):
        os.remove('test_cars_result.csv')
    np.random.seed(0)
    num_lines = sum(1 for line in open('tests/car.data'))
    file = open('tests/car.data', 'r')
    array = []

    for i in range(0, num_lines):
        line = file.readline().split(',')
        line = [x.strip() for x in line]
        array.append(line)

    cars_data = {'data': [a[:-1] for a in array],
                 'data_factorized' : None,
                 'buying': np.array(pd.factorize([a[0] for a in array])[0]),
                 'buying_names' : np.array(list(set([a[0] for a in array])), dtype='<U10'),
                 'maint': np.array(pd.factorize([a[1] for a in array])[0]),
                 'maint_names' : np.array(list(set([a[1] for a in array])), dtype='<U10'),
                 'doors': np.array(pd.factorize([a[2] for a in array])[0]),
                 'doors_names' : np.array(list(set([a[2] for a in array])), dtype='<U10'),
                 'persons': np.array(pd.factorize([a[3] for a in array])[0]),
                 'persons_names' : np.array(list(set([a[3] for a in array])), dtype='<U10'),
                 'lug_boot': np.array(pd.factorize([a[4] for a in array])[0]),
                 'lug_boot_names' : np.array(list(set([a[4] for a in array])), dtype='<U10'),
                 'safety': np.array(pd.factorize([a[5] for a in array])[0]),
                 'safety_names' : np.array(list(set([a[5] for a in array])), dtype='<U10'),
                 'car' : np.array(pd.factorize([a[-1] for a in array])[0]),
                 'car_names' : np.array(list(set([a[-1] for a in array])), dtype='<U10'),
                 'feature_names' : ['buying', 'maint', 'doors', 'persons', 'lug_boot', 'safety']}
    cars_data['data_factorized'] = np.column_stack((cars_data['buying'], cars_data['maint'], cars_data['doors'],
                                                cars_data['persons'], cars_data['lug_boot'],cars_data['safety']))

    df = pd.DataFrame(cars_data['data_factorized'], columns=cars_data['feature_names'])
    df['categories'] = cars_data['car']
    df['is_train'] = np.random.uniform(0,1,len(df)) <= 0.75
    train, test = df[df['is_train'] == True], df[df['is_train'] == False]

    features = df.columns[:6]
    clf = RandomForestClassifier(n_jobs=2, random_state=0)
    clf.fit(train[features], train['categories'].values)
    predicted = cars_data['car_names'][clf.predict(test[features])]
    predicted = pd.crosstab(cars_data['car_names'][test['categories']], predicted, rownames=['Oryginalne'], colnames=['Przewidziane'])

    file = open('test_cars_result.csv', 'a+')
    file.write('\n\nRESULT - CARS\n\n\nSKLEARN\n')
    predicted.to_csv(file, sep='\t', encoding='utf-8')

    print("\n\nRESULT - CARS\n\n")
    print("\nSKLEARN\n")
    print(predicted)

    features_and_categories = df.columns[:7]
    random_forest_cars = RandomForest(train[features_and_categories].values)
    random_forest_cars.train()
    predict = []

    for t in test[features].values:
        predict.append(cars_data['car_names'][random_forest_cars.predict(t)])

    predict = np.array(predict, dtype='<U10')

    tab_car = pd.crosstab(cars_data['car_names'][test['categories']], predict, rownames=["Oryginalne"], colnames=["Przewidywane"])

    file.write('\nNASZE\n')
    tab_car.to_csv(file, sep='\t', encoding='utf-8')
    file.close()
    print("\n\nNASZE\n")
    print(tab_car)
