
__author__ = "Tomasz Pyrek, Marika Wróbel"
__copyright__ = "Tomasz Pyrek, Marika Wróbel"
__license__ = "mit"

class Node:
    """
    Class that represents tree nodes used to build tree
    """
    def __init__(self, data):
        """
        Constructor of Node class
        :param data: training data which is held by this node
        """
        self.data = data

        # If node is not a leaf
        self.left_child_node = None
        self.right_child_node = None
        self.split_point = None

        # If node is the leaf what means that we can not split data
        self.category = None
