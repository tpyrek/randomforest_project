# -*- coding: utf-8 -*-

import numpy
from random_forest.Node import Node

__author__ = "Tomasz Pyrek, Marika Wróbel"
__copyright__ = "Tomasz Pyrek, Marika Wróbel"
__license__ = "mit"

class RandomForest:
    """
    Class that represents RandomForest
    """

    def __init__(self, training_data, number_of_trees=5, max_depth_of_tree=8, min_data_size_to_split=1, samples_rate=90):
        """
        Constructor of RandomForest class
        Required training_data format:

                feature1    feature2    feature3    ...     featuren    category

        row1        a           b           c                   d         cat1
        row2        e           f           g                   h         cat2
        .
        .
        .

        rown        i           j           k                   l         cat3

        where a-l are values of features for example some dimensions and cat1-cat3 are our classified objects for example
        type of flowers or cars.

        Args:
            training_data (array): training data to build trees - TWO_DIMENSIONAL ARRAY - do not pass arrays with names of
                features and rows
            number_of_trees (int): number of trees used by algorithm to predict, Default = 5
            max_depth_of_tree (int): maximum number of nodes in tree, Default = 8
            min_data_size_to_split (int): minimum amount of data to be split, Default = 1
            samples_rate (int): percent of training data which is used in training

        """
        self.number_of_trees = number_of_trees
        self.max_depth_of_tree = max_depth_of_tree
        self.min_data_size_to_split = min_data_size_to_split
        self.samples_rate = samples_rate/100
        self.training_data = training_data
        
        self.training_data_samples = int(len(self.training_data) * self.samples_rate)
        self.number_of_features = self.calculate_number_of_features()
        self.total_number_of_features = len(self.training_data[0]) - 1
        self.categories = self.get_categories_from_training_data()
        self.trees = []

    def calculate_number_of_features(self):
        """
        Function that calculates number of features used to build tree - sqrt of all features - in this way features
        used to build each trees are different so the final trees will be different

        Returns:
            int: number of features used to build tree
        """
        return int((len(self.training_data[0]) - 1) ** (1 / 2))

    def get_features_from_training_data(self):
        """
        Function that randomly choose number_of_features features from total number of given features. This increase
        the chance, that the built trees will be different

        Returns:
            'Randomly' chosen features
        """

        # Array with values from 0 to total number of features
        features = [i for i in range(0, self.total_number_of_features)]
        numpy.random.shuffle(features)  # Shuffle for better randomness of built trees
        return features[:self.number_of_features]  # Return features[0 - number_of_features]

    def get_categories_from_training_data(self):
        """
        Function that get categories included in training data (last column)

        Returns:
            Categories from training data
        """
        # By set function categories are not replicated if there is more then one in last training data column
        return set([row[-1] for row in self.training_data])

    def get_category_for_leaf(self, current_data_in_node):
        """
        Function that get the category, which occurs most in training data in node when it is not possible to next
        split. This category is assigned to node.

        Args:
            current_data_in_node: Data which should be held by the node

        Returns:
            category for leaf
        """
        categories_in_data_in_node = [row[-1] for row in current_data_in_node]
        return max(self.categories, key=categories_in_data_in_node.count)

    def find_the_best_split_point(self, data):
        """
        Function that choose split point based on gini index, the lower value of this index, the better it is.
        When calculating gini index, the algorithm check every possible variances : divides training data into groups
        and consider randomly chosen features by get_features_from_training_data function
        :param data: array in which we want to fint the bes split point
        :return:

        Args:
            data: array in which we want to fint the bes split point

        Returns:
            int: x, y - respectively x row of training data, y column of training data
        """
        features = self.get_features_from_training_data()
        x = None
        y = None
        gini_index = None
        for row in range(0, len(data)):
            for column in features:
                group1, group2 = self.split_data(data, row, column)  # Split training data
                new_gini_index = self.calculate_gini_index(group1, group2, self.categories)  # Calculate gini index

                if gini_index is None or new_gini_index < gini_index:  # Find the lowest index
                    x = row
                    y = column
                    gini_index = new_gini_index

        return x, y

    def split_data(self, data, row, column):
        """
        Function that divide training data into two groups considering feature: for example the height is the future
        and row and column indicate height (feature) = 1.7m so first group include rows with height smaller or equal
        1.7m, second group include rows with height grater than 1.7m

        Args:
            data: data to split
            int: row: row of training data
            int: column: column of training data which represent feature

        Returns:
            group1, group2 - training data divided into two groups
        """
        split_value = data[row][column]
        group1 = []
        group2 = []

        # For all rows in data array
        for row in data:
            if row[column] > split_value:
                group2.append(row)
            else:
                group1.append(row)

        return group1, group2

    def calculate_gini_index(self, group1, group2, categories):
        """
        Function that calculates Gini index based on the following formula:
        gini = [1 - (number_of_categories[0]_in_group_1/quantity_of_group_1)^2 -
        (number_of_categories[1]_in_group_1/quantity_of_group_1)^2 - ...] * (quantity_of_group_1/(quantity_of_group_1 + quantity_of_group_2)) +
        [1 - (number_of_categories[0]_in_group_2/quantity_of_group_2)^2 -
        (number_of_categories[1]_in_group_2/quantity_of_group_2)^2 - ...] * (quantity_of_group_2/(quantity_of_group_1 + quantity_of_group_2))

        Args:
            group1:  first group of training data after split
            group2:  second group of training data after split
            categories:  names what we want to predict for example names of flours or gender

        Returns:
            float: calculated gini index
        """

        gini_index = 0

        if len(group1) != 0:
            score = 1

            # For all categories
            for category in categories:
                # Number of occurrences of category in group in relation to all elements in this group
                proportion = [row[-1] for row in group1].count(category) / len(group1)
                power_of_proportion = proportion ** 2
                score -= power_of_proportion

            gini_index += score * (len(group1) / len(group1 + group2))

        if len(group2) != 0:
            score = 1

            # For all categories
            for category in categories:
                # Number of occurrences of category in group in relation to all elements in this group
                proportion = [row[-1] for row in group2].count(category) / len(group2)
                power_of_proportion = proportion ** 2
                score -= power_of_proportion

            gini_index += score * (len(group2) / len(group1 + group2))

        return gini_index

    def build_tree(self, data, current_depth):
        """
        Function that build tree.

        Args:
            data: training data
            int: current_depth: Current depth when building the tree

        Returns:
            Node: tree node

        """

        root_node = Node(data)
        row, column = self.find_the_best_split_point(data)  # Calculate the best split point which give the smallest gini index
        group1, group2 = self.split_data(data, row, column)  # Split data

        # If one of groups is empty or max_depth_of_tree is achieved the node is a leaf and it is not possible
        # further training data division
        if len(group1) == 0 or len(group2) == 0 or current_depth >= self.max_depth_of_tree:
            root_node.category = self.get_category_for_leaf(group1 + group2)  # Assign category to leaf node

        else:
            root_node.split_point = (row, column)  # Assign split point to node

            # If the splitted group is too small for further division
            if len(group1) < self.min_data_size_to_split:
                root_node.left_child_node = Node(group1)
                root_node.left_child_node.category = self.get_category_for_leaf(group1)

            # Recursion - build next tree nodes
            else:
                root_node.left_child_node = self.build_tree(group1, current_depth + 1)

            # If the splitted group is too small for further division
            if len(group2) < self.min_data_size_to_split:
                root_node.right_child_node = Node(group2)
                root_node.right_child_node.category = self.get_category_for_leaf(group2)

            # Recursion - build next tree nodes
            else:
                root_node.right_child_node = self.build_tree(group2, current_depth + 1)

        return root_node

    def train(self):
        """
        Function that create array with built trees.
        """
        for i in range(0, self.number_of_trees):
            # Build tree with starting depth 1
            # For each tree shuffle training data to increase the chance that each tree will be different
            numpy.random.shuffle(self.training_data)
            # Build tree using part of training data - it is also to increase chance that each tree will be different
            tree = self.build_tree(self.training_data[:self.training_data_samples], 1)
            self.trees.append(tree)

    def predict_based_on_one_tree(self, node, data_to_predict_row):
        """
        Function that get the data witch we want to predict and based on built one tree try to classify it.

        Args:
            Node: node: node of tree
            data_to_predict_row: Data which we want to classify

        Returns:
            predicted category for data_to_predict_row

        """

        # If node is a leaf return its category
        if node.category is not None:
            return node.category

        # If no, get the split point and check if data is less or grater than value on split point
        # and go to left or right child
        row, column = node.split_point
        split_value = node.data[row][column]

        if data_to_predict_row[column] > split_value:
            return self.predict_based_on_one_tree(node.right_child_node, data_to_predict_row)
        else:
            return self.predict_based_on_one_tree(node.left_child_node, data_to_predict_row)

    def predict(self, data_to_predict_row):
        """
        Function that try to classify data which we want to predict based on built trees. Function return predicted
        category which occurs most in trees predictions.
        Required data_to_predict_row format:

                feature1    feature2    feature3    ...     featuren

        row1        a           b           c                   d

        where a-d are values of features for example some dimensions.

        Args:
            data_to_predict_row: Data which we wan to to classify - ONE ROW - do not pass arrays with features and
                row descriptions

        Returns:
            predicted category for data_to_predict_row which occurs most in trees predictions
        """
        predictions = []
        for tree in self.trees:
            predictions.append(self.predict_based_on_one_tree(tree, data_to_predict_row))

        # Return category which occurs most
        return max(set(predictions), key=predictions.count)
