=============
random_forest
=============


Add a short description here!


Description
===========

A longer description of your project goes here...


Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

Tests
=====

To run all test use 
```python3 setup.py test```
from the main directory. Results of all test are in files named : TEST_NAME_result.csv

Data for tests got from: http://archive.ics.uci.edu/ml/index.php?fbclid=IwAR2B8ldo9ClBRT40EvxNnD8MgdtUOz8p4u0WGlLjIMFEvTlXcCteie-DJl8
